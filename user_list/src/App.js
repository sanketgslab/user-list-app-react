import React, { useState } from 'react';
import './App.css';
import Login from './components/Login'
import UserList from './components/UserList';
import Protected from './components/Protected';
import UserDetails from './components/UserDetails'
import { Switch, Route, Redirect, BrowserRouter } from "react-router-dom";


function App() {

  return (
    <div className="App">
      <BrowserRouter basename="/">
        <Switch>
          <Route path="/userlist"> <Protected cmp={UserList} /> </Route>
          <Route path={"/userDetails"} component={UserDetails}></Route>
          <Route path='/' component={Login} />
          <Redirect to='/' />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
