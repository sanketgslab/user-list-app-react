import React, { useState } from 'react'
import { useHistory } from "react-router-dom";
const axios = require('axios')


function Login() {

  let history = useHistory()
  const [state, setstate] = useState({ username: '', password: '' })
  const [error, setError] = useState(false)

  const handleChangeEvents = (e) => {
    setstate(prevState => ({ ...prevState, [e.target.name]: e.target.value }))
    setError(false)
  }

  const handleLogin = (e) => {
    e.preventDefault()
    let data = {
      "email": state.username,
      "password": state.password
    }
    axios.post('https://reqres.in/api/login', data)
      .then((response) => {
        let item = response && response.data
        if (item && item.token) {
          localStorage.setItem("token", JSON.stringify(item.token))
          history.push('/userlist')
        }
      }).catch(err => {
        console.log(err)
        setstate({ username: '', password: '' })
        setError(!error)
      })

  }

  return (
    <div>
      <form onSubmit={(e) => handleLogin(e)}>
        {error ? <h2 style={{ color: "red" }}>Wrong Id And Password</h2> : ''}
        <label>User Name</label>
        <input type="text" name="username" value={state.username} onChange={handleChangeEvents} />
        <label>Password</label>
        <input type="password" name="password" value={state.password} onChange={handleChangeEvents} />
        <input type="submit" value="Log In" name="submit" />
      </form>
    </div>
  )
}

export default Login;