import React, { Component } from 'react';
import { Redirect } from "react-router-dom";

function Protected(props) {
  const Cmp = props.cmp;
  let token = JSON.parse(localStorage.getItem('token'));
  return <div> {token ? <Cmp /> : <Redirect to="/"></Redirect>}</div>
}

export default Protected;