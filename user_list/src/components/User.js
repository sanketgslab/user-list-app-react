import React, { useState } from 'react';
import './User.css';
import { Link, useRouteMatch, useHistory } from 'react-router-dom';

function User(props) {

  let history = useHistory()
  let id = props.data.id
  let { path, url } = useRouteMatch();
  console.log('Users', props)

  function getData(e) {
    e.preventDefault();
    history.push({
      pathname: '/userDetails',
      state: {
        data: props.data
      }
    });

  }

  return (
    <div className="alignBox">

      <Link to={`/userDetails`}>
        {console.log("URL", url, "URL ID", id)}
        <div className="card" onClick={(e) => getData(e)}>
          <img src="https://picsum.photos/200" />
          <div className="container">
            <p>Key={props.data.id}</p>
            <p>Name: {props.data.name}</p>
            <h4>Email: <b>{props.data.email}</b></h4>
            <p>Phone: {props.data.phone}</p>
          </div>
        </div>
      </Link>
      <div>
      </div>
    </div>
  )
}
export default User
