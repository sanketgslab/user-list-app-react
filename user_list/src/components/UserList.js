import React, { useState, useEffect } from 'react';
import User from './User';
import './UserList.css';


function UserList() {
  const [state, setState] = useState([])

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => setState(...state, json))
  }, [])

  return (
    <div>
      {
        <div className='boxAlign'>
          {
            state.map((data, i) => (
              <User key={i} data={data} />)
            )
          }
        </div>
      }
    </div>
  )
}

export default UserList
