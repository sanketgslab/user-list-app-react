import React from 'react';


function UserDetails(props) {
  let data = props && props.location && props.location.state && props.location.state.data;
  return (
    <div>
      {data ?
        <div>
          <h1>User Details Page</h1>
          <p>{data.name}</p>
          <p>{data.email}</p>
          <p>{data.phone}</p>
        </div>
        :
        <div>
          NO DATA FOUND
      </div>}
    </div>
  )
}

export default UserDetails
