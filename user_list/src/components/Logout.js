import React from 'react'

function Logout() {
  const logout = () => {
    localStorage.clear();
    window.location.href = '/';
  }
  return (
    <div>
      {/* <h1>Sorry, you have been logged out. Please Login Again</h1> */}
      <button onClick={logout}>Logout</button>
    </div>
  )
}

export default Logout
