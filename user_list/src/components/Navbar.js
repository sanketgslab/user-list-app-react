import React from 'react'
import { Link } from 'react-router-dom'

function Navbar() {
    return (
        <nav>
            <ul>
                {/* <li><Link to='/userlist'>Home</Link></li> */}
                <li style={{textDecoration:'none'}}><Link to='/'>Login Page</Link></li>
            </ul>
        </nav>
    )
}

export default Navbar
